

	
Items 140508:
-------------

	- When analytic support (i.e. record splitting) is added in, need to deal with report_artbecomedoc() which shows c. 17,000 records that should be treated as *doc and so should not be split, but their parentage needs to be evident (i.e. 
          currently cannot tell that it has been extracted from something else)

		- Requires note "Extracted from <*tg/*t> <*d> <*pt>"

		- Same with report_artlocationpam(); this identifies pamphlets which need to be loaded into Voyager as documents (not as an article), but where the parent title needs to be made explicit in the record itself - i.e. is cases where 
                  SPRI doesn't subscribe to the journal so will never have a match - is standalone offprint




